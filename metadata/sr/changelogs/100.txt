# в4.2.0 - Ashes to Ashes (Издање #100)

**2024-10-04**
- поправља пребацивање станица (дугмад за обавештења и слушалице)
- поправља наставак репродукције из обавештења
- мање исправке грешака
- нови превод: ирски (ХВАЛА Aindriú Mac Giolla Eoin)
- нови превод: велшки (ХВАЛА Aled Powell)
- ажурирани преводи
