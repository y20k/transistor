# versioon 4.1.1 - Modern Love

**2022-05-12**

- lisasime võimaluse varundada ja taastada raadiojaamade info koos kaanepiltidega 
- parandasime vaikimisi kuvatava hägusa kaanepildi
- parandasime indoneesia keele vead
- uuendasime tõlkeid
