# v4.0.3 - Andy Warhol

**2020-07-22**

- Android Auto wznawia odtwarzanie ostatnio odtwarzanej stacji
- zaktualizowane i nowe tłumaczenia
- kilka drobnych poprawek błędów

Aktualizacja v4 była dość duża, dowiedz się więcej: https://codeberg.org/y20k/transistor/src/branch/master/metadata/en-US/changelogs/72.txt
