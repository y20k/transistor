# v3.1.0 - John, I’m Only Dancing

**2018-11-19**

- nowa ikona aplikacji (thx @munadikieh)
- ulepszona obsługa linków z radio-browser.info
- interfejs użytkownika z nowym schematem kolorów
- poprawki błędów
