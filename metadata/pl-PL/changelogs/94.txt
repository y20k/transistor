# v4.1.4 - Modern Love

**2023-03-01**

- Naprawiono odtwarzanie M3U8/HLS
- Dodano wariant monochromatycznej ikony aplikacji
- Utrata połączenia sieciowego jest lepiej obsługiwana
